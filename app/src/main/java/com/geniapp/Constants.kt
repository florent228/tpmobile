package com.geniapp

object Constants {

    fun getQuestions(): ArrayList<Question> {
        val questionsList = ArrayList<Question>()

        // 1
        val que1 = Question(
            1, "Quelle est la formation que propose Ynov Campus  ?",
            R.drawable.ic_ynov,
            "Informatique", "Comptabilité",
            "Anglais", "lettres modernes", 1
        )

        questionsList.add(que1)

        // 2
        val que2 = Question(
            2, "Quelle est la capitale de la France?",
            R.drawable.ic_fr,
            "Toulouse", "Nice",
            "Paris", "Lome", 3
        )

        questionsList.add(que2)

        // 3
        val que3 = Question(
            3, "Quel est le nom de ce joli bébé?",
            R.drawable.ic_zabou,
            "Alice", "Josephe",
            "Florent", "Zabou", 4
        )

        questionsList.add(que3)

        // 4
        val que4 = Question(
            4, "Dans quoi Eve a-t-elle croqué ??",
            R.drawable.ic_quizz,
            "Un bras", "Une pomme",
            "Une tomate", "Du pain", 2
        )

        questionsList.add(que4)

        // 5
        val que5 = Question(
            5, "Combien d'animaux de chaque sexe Moïse emmena t-il sur son arche ?",
            R.drawable.ic_quizz,
            "0", "1",
            "2", "3", 3
        )

        questionsList.add(que5)

        // 6
        val que6 = Question(
            6, "Qui était le dieu de la guerre dans la mythologie grecque?",
            R.drawable.ic_quizz,
            "Ares", "Appolon",
            "Atcheakou", "chichi", 1
        )

        questionsList.add(que6)

        // 7
        val que7 = Question(
            7, "Comment s'appel le fils de Sangoku?",
            R.drawable.ic_goku,
            "Zemour", "Atachi",
            "Marine", "gohan", 3
        )

        questionsList.add(que7)

        // 8
        val que8 = Question(
            8, " À combien de kilos correspond une tonne?",
            R.drawable.ic_quizz,
            "1024 Kg", "100 Kg",
            "100000", "1000 Kg", 4
        )

        questionsList.add(que8)

        // 9
        val que9 = Question(
            9, "Pour moi, qui est l'épouse de mon frère?",
            R.drawable.ic_quizz,
            "Ma tante", "Ma belle-sœur",
            "Tuvalu", "Ma sœur", 2
        )

        questionsList.add(que9)

        // 10
        val que10 = Question(
            10, "Qui est le concepteur de GeniApp?",
            R.drawable.ic_florent,
            "Florent", "Francois",
            "Alice", "Sophie", 1
        )

        questionsList.add(que10)

        return questionsList
    }
}